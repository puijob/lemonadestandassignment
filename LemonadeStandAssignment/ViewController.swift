//
//  ViewController.swift
//  LemonadeStandAssignment
//
//  Created by Costinescu on 28/06/15.
//  Copyright (c) 2015 Costinescu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var inventoryLemonsLabel: UILabel!
    @IBOutlet weak var inventoryIceCubesLabel: UILabel!
    @IBOutlet weak var purchaseLemonsCountLabel: UILabel!
    @IBOutlet weak var purchaseIceCubesCountLabel: UILabel!
    @IBOutlet weak var mixLemonsCountLabel: UILabel!
    @IBOutlet weak var mixIceCubesCountLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    
    let lemonadePrice = 1
    
    var credits = 10
    var lemons = Supply()
    var iceCubes = Supply()
    var lemonadeMixRatio: Double = 0.0
    
    var customers: [Customer] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Initialize lemons supplies
        lemons.name = "lemons"
        lemons.price = 2
        lemons.inventory = 1
        
        inventoryLemonsLabel.text = "\(lemons.inventory)"
        
        // Initialize ice cubes supplies
        iceCubes.name = "iceCubes"
        iceCubes.price = 1
        iceCubes.inventory = 1
        
        inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
        
        // Update weather icon
        updateWeatherIcon()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Purchase more lemons action
    @IBAction func purchaseMoreLemonsButtonPressed(sender: AnyObject) {

        // Check if there are enogh money to purchase lemons
        if credits < lemons.price {
            showAlertWithText(header: "Not enough money", message: "You need at least $\(lemons.price) to purchase \(lemons.name)")
        }
        else {
            lemons.purchase += 1
            lemons.inventory += 1
            credits -= lemons.price
            
            // Update labels
            inventoryLemonsLabel.text = "\(lemons.inventory)"
            purchaseLemonsCountLabel.text = "\(lemons.purchase)"
            creditsLabel.text = "\(credits)"
        }
    }
    
    // Un-purchase lemons action
    @IBAction func purchaseLessLemonsButtonPressed(sender: AnyObject) {
        
        // Check if there are anymore lemons to give back
        // Also check if the lemon trying to give back isn't put away for the lemonade
        if lemons.purchase > 0 && lemons.inventory > 0 {
            lemons.purchase -= 1
            lemons.inventory -= 1
            credits += lemons.price
            
            // Update labels
            inventoryLemonsLabel.text = "\(lemons.inventory)"
            purchaseLemonsCountLabel.text = "\(lemons.purchase)"
            creditsLabel.text = "\(credits)"
        }
        else if lemons.purchase > 0 {
            showAlertWithText(message: "If you want to purchse fewer \(lemons.name), don't use them in your mix")
        }
    }
    
    // Purchase more ice cubes action
    @IBAction func purchaseMoreIceCubesButtonPressed(sender: AnyObject) {
        
        // Check if there are enogh money to purchase ice cubes
        if credits < iceCubes.price {
            showAlertWithText(header: "Not enough money", message: "You need at least $\(iceCubes.price) to purchase \(iceCubes.name)")
        }
        else {
            iceCubes.purchase += 1
            iceCubes.inventory += 1
            credits -= iceCubes.price
            
            // Update labels
            inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
            purchaseIceCubesCountLabel.text = "\(iceCubes.purchase)"
            creditsLabel.text = "\(credits)"
        }
    }
    
    // Un-purchase ice cubes action
    @IBAction func purchaseLessIceCubesButtonPressed(sender: AnyObject) {
        
        // Check if there are anymore ice cubes to give back
        // Also check if the ice cube trying to give back isn't put away for the lemonade
        if iceCubes.purchase > 0 && iceCubes.inventory > 0 {
            iceCubes.purchase -= 1
            iceCubes.inventory -= 1
            credits += iceCubes.price
            
            inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
            purchaseIceCubesCountLabel.text = "\(iceCubes.purchase)"
            creditsLabel.text = "\(credits)"
        }
        else if iceCubes.purchase > 0 {
            showAlertWithText(message: "If you want to purchse fewer \(iceCubes.name), don't use them in your mix")
        }
    }
    
    // Add more lemons to lemonade action
    @IBAction func mixMoreLemonsButtonPressed(sender: AnyObject) {
        
        // Check if there are enough lemons
        if lemons.inventory == 0 {
            showAlertWithText(header: "No more \(lemons.name)", message: "Please purchase more \(lemons.name)")
        }
        else {
            lemons.mix += 1
            lemons.inventory -= 1
            
            // Update labels
            inventoryLemonsLabel.text = "\(lemons.inventory)"
            mixLemonsCountLabel.text = "\(lemons.mix)"
        }
    }
    
    // Use fewer lemons for lemonade action
    @IBAction func mixLessLemonsButtonPressed(sender: AnyObject) {
        
        // Check if there are still lemons added for lemonade
        if lemons.mix > 0 {
            lemons.mix -= 1
            lemons.inventory += 1
            
            // Update labels
            inventoryLemonsLabel.text = "\(lemons.inventory)"
            mixLemonsCountLabel.text = "\(lemons.mix)"
        }
    }
    
    // Add more ice cubes to lemonade action
    @IBAction func mixMoreIceCubesButtonPressed(sender: AnyObject) {
        
        // Check if there are enough ice cubes
        if iceCubes.inventory == 0 {
            showAlertWithText(header: "No more \(iceCubes.name)", message: "Please purchase more \(iceCubes.name)")
        }
        else {
            iceCubes.mix += 1
            iceCubes.inventory -= 1
            
            // Update labels
            inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
            mixIceCubesCountLabel.text = "\(iceCubes.mix)"
        }
    }
    
    // Use fewer ice cubes for lemonade action
    @IBAction func mixLessIceCubesButtonPressed(sender: AnyObject) {
        
        // Check if there are still ice cubes added for lemonade
        if iceCubes.mix > 0 {
            iceCubes.mix -= 1
            iceCubes.inventory += 1
            
            // Update labels
            inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
            mixIceCubesCountLabel.text = "\(iceCubes.mix)"
        }
    }
    
    // Start day action
    @IBAction func startDayButtonPressed(sender: AnyObject) {
        
        // Check if lemonade is ready to be prepared
        if checkMixForLemons() {
            
            // Calculate lemonade mix ratio
            calculateLemonadeMixRatio()
            
            // Generate customers for the day
            customers = Factory.generateCustomers()
            
            println("Your lemonade mix ratio is \(lemonadeMixRatio) (\(lemons.mix) \(lemons.name) to \(iceCubes.mix) \(iceCubes.name))")
            
            // Check which customers like the lemonade and which don't
            for (index, customer) in enumerate(customers) {
                print("Customer \(index + 1) prefered mix ratio: \(customer.preferedMixRatio) - ")
                
                if CustomerBrain.checkMixRatioMatch(customer, lemonadeMixRatio: lemonadeMixRatio) {
                    credits += lemonadePrice
                    
                    print("Paid! \n")
                }
                else {
                    print("No match, No Revenue. \n")
                }
            }
            
            // Update credits label
            creditsLabel.text = "\(credits)"
            
            // Reset purchases and mix
            // Also update weather icon
            resetPurchases()
            resetMix()
            updateWeatherIcon()
            
            // Check if game is over
            if lemons.inventory < 1 && credits < lemons.price {
                showAlertWithText(header: "Game Over!", message: "You are out of lemons and can't afford to buy more. No worries, your game will reset now.")
                resetGame()
            }
        }
        else {
            showAlertWithText(message: "You can't do lemonade without lemons. Please add at least 1 lemon to your mix.")
        }
    }
    
    // Check if lemonade is ready to be prepared
    func checkMixForLemons() -> Bool {
        
        // If there is at least 1 lemon, lemonade can be prepared
        if lemons.mix >= 1 {
            return true
        }
        else {
            return false
        }
    }
    
    // Calculate lemonade mix ratio of lemons over ice cubes
    func calculateLemonadeMixRatio() {
        
        // Check for ice cubes used
        if iceCubes.mix == 0 {
            lemonadeMixRatio = 1000.0
        }
        else {
            lemonadeMixRatio = Double(lemons.mix) / Double(iceCubes.mix)
        }
    }
    
    // Reset purchases function
    func resetPurchases() {
        
        lemons.purchase = 0
        iceCubes.purchase = 0
        
        // Update labels
        purchaseLemonsCountLabel.text = "\(lemons.purchase)"
        purchaseIceCubesCountLabel.text = "\(iceCubes.purchase)"
    }
    
    // Reset lemonade mix function
    func resetMix() {
        
        lemons.mix = 0
        iceCubes.mix = 0
        
        // Update labels
        mixLemonsCountLabel.text = "\(lemons.mix)"
        mixIceCubesCountLabel.text = "\(iceCubes.mix)"
    }
    
    // Update weather icon function
    func updateWeatherIcon() {
        
        // Generate weather
        var weather = Factory.generateWeather()
        
        switch weather {
        case "cold":
            weatherIcon.image = UIImage(named: "Cold")
        case "warm":
            weatherIcon.image = UIImage(named: "Warm")
        default:
            weatherIcon.image = UIImage(named: "Mild")
        }
    }
    
    // Reset game function
    func resetGame() {

        credits = 10
        lemons.inventory = 1
        iceCubes.inventory = 1
        
        // Update labels
        creditsLabel.text = "\(credits)"
        inventoryLemonsLabel.text = "\(lemons.inventory)"
        inventoryIceCubesLabel.text = "\(iceCubes.inventory)"
    }
    
    // Show alert function
    func showAlertWithText(header:String = "Warning", message:String) {
        var alert = UIAlertController(title: header, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

}

