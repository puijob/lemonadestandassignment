//
//  Customer.swift
//  LemonadeStandAssignment
//
//  Created by Costinescu on 30/06/15.
//  Copyright (c) 2015 Costinescu. All rights reserved.
//

import Foundation

struct Customer {
    
    var preferedMixRatio: Double = 0.0
}
