//
//  Factory.swift
//  LemonadeStandAssignment
//
//  Created by Costinescu on 30/06/15.
//  Copyright (c) 2015 Costinescu. All rights reserved.
//

import Foundation

class Factory {
    
    // Generate customers function
    class func generateCustomers() -> [Customer] {
        
        // Generate a random number of customers for the day
        let randomNumber = Int(arc4random_uniform(UInt32(10))) + 1
        // Genarate day weather
        let weather = generateWeather()
        
        var customers: [Customer] = []
        var customersNumberByWeather = 0
        var customersNumberTotal = 0
        
        // Check for weather and update the number of customers
        // weather is contributing with which can also be negative
        switch weather {
        case "cold":
            customersNumberByWeather = -3
        case "warm":
            customersNumberByWeather = 4
        default:
            customersNumberByWeather = 0
        }
        
        // Calculate total number of customers for the day
        customersNumberTotal = randomNumber + customersNumberByWeather
        
        // Check if there are any customers
        if customersNumberTotal > 0 {
            print("\nYou've got \(customersNumberTotal) customers today! Weather was \(weather) and influenced the initial number of \(randomNumber) customers by \(customersNumberByWeather)\n")
        }
        else {
            print("\nBummer! Looks like weather scared away all your customers today :(\n")
        }
        
        // Generate each customer independently
        for var index = 0; index < customersNumberTotal; ++index {
            var customer = generateCustomer()
            
            customers.append(customer)
        }
        
        // Function returns the customers array
        return customers
    }
    
    // Generate customer function
    class func generateCustomer() -> Customer {
        
        // Generate a random number between 0 and 1
        // representing customer preference for the lemonade mix
        // 0 to 0.4 – favors acidic lemonade
        // 0.4 to 0.6 – favors equal parts lemonade
        // 0.6 to 1 – favors diluted lemonade
        let randomNumber = Double(arc4random_uniform(UInt32(11))) / 10
        
        var customer: Customer = Customer(preferedMixRatio: randomNumber)
        
        return customer
    }
    
    // Generate weather function
    class func generateWeather() -> String {
        
        // Generate a random number between 0 and 2
        // representing a weather setting
        // 0 is cold
        // 1 is mild
        // 2 is warm
        let randomNumber = Int(arc4random_uniform(UInt32(3)))
        
        var weather = ""
            
        if randomNumber < 1 {
            weather = "cold"
        }
        else if randomNumber > 1 {
            weather = "warm"
        }
        else {
            weather = "mild"
        }
        
        return weather
    }
}
