//
//  Supply.swift
//  LemonadeStandAssignment
//
//  Created by Costinescu on 29/06/15.
//  Copyright (c) 2015 Costinescu. All rights reserved.
//

import Foundation

struct Supply {

    var name = ""
    var price = 0
    var inventory = 0
    var purchase = 0
    var mix = 0
    var message = ""
}
