//
//  CustomerBrain.swift
//  LemonadeStandAssignment
//
//  Created by Costinescu on 30/06/15.
//  Copyright (c) 2015 Costinescu. All rights reserved.
//

import Foundation

class CustomerBrain {
    
    // Compare lemonade mix ration to customers preferences
    class func checkMixRatioMatch(customer: Customer, lemonadeMixRatio: Double) -> Bool {
        
        let favoursBalancedMin = 0.4
        let favoursBalancedMax = 0.6
        
        if customer.preferedMixRatio < favoursBalancedMin && lemonadeMixRatio > 1 {
            // Customer favours acidic lemonades and matches our lemonade
            return true
        }
        else if customer.preferedMixRatio > favoursBalancedMax && lemonadeMixRatio < 1 {
            // Customer favours diluted lemonades and matches our lemonade
            return true
        }
        else if customer.preferedMixRatio >= favoursBalancedMin && customer.preferedMixRatio <= favoursBalancedMax && lemonadeMixRatio == 1 {
            // Customer favours balanced lemonades and matches our lemonade
            return true
        }
        else {
            // No match
            return false
        }
    }
}
